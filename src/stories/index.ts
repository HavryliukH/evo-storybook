import { storiesOf } from '@storybook/angular';
import { action } from '@storybook/addons';
import { ButtonComponent } from '../app/components/button/button.component';

storiesOf('Button', module)
    .add('With content', () => ({
        component: ButtonComponent,
        props: {
            text: '👍'
        }
    }))
